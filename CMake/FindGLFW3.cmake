#
# Try to find GLFW library and include path.
# Once done this will define
#
# GLFW3_FOUND
# GLFW3_INCLUDE_PATH
# GLFW3_LIBRARY
# 

IF (GLFW3_INCLUDE_PATH)
	SET( GLFW3_FOUND 1 CACHE STRING "Set to 1 if GLFW3 is found, 0 otherwise")
ELSE (GLFW3_INCLUDE_PATH)
	SET( GLFW3_FOUND 0 CACHE STRING "Set to 1 if GLFW3 is found, 0 otherwise")
ENDIF (GLFW3_INCLUDE_PATH)

MARK_AS_ADVANCED( GLFW3_FOUND )
