#version 330 core
layout (location = 0) in vec3 aPos;
layout (location = 1) in vec3 aNormal;
layout (location = 2) in vec2 aTexCoords;

out vec2 TexCoords;
out vec3 vPosition_vs;
out vec3 vNormal_vs;

/*uniform mat4 model;
uniform mat4 view;
uniform mat4 projection;*/

uniform mat4 uMVPMatrix;
uniform mat4 uMVMatrix;
uniform mat4 uNormalMatrix;

void main()
{
    vec4 vertexPosition = vec4(aPos, 1);
    vec4 vertexNormal = vec4(aNormal, 0);

  	//Valeurs de sortie
  	vPosition_vs = vec3(uMVMatrix * vertexPosition);
    vNormal_vs = vec3(uNormalMatrix * vertexNormal);
    TexCoords = aTexCoords;

    //Position projetée
    gl_Position = uMVPMatrix * vertexPosition;
    //gl_Position = projection * view * model * vec4(aPos, 1.0);
}
