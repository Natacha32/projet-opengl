#ifndef SCENE_HPP
#define SCENE_HPP

#include "Model.hpp"

namespace ourLib {

class Openable {

    public:

    bool isOpen;
    std::string name;
    std::vector<Model> open;
    std::vector<Model> closed;
    
    void openOrClose(int comportement);

};

class Key {
    
    public :

    bool isTaken;
    std::vector<Model> model;

    void takeKey();
};

class Scene {

    public:

    std::vector<Model> base;
    std::vector<Openable> openables;
    Key key;

    // constructor
    Scene(std::string path, std::string base_name, std::vector<std::string> openables_name, std::string key_name);
    // destructor
    ~Scene();

    // draw the scene
    void draw(Shader shader);

};

}

#endif
