#include "../include/Camera.hpp"
#include <iostream>

namespace ourLib {

bool Camera::_singleton = false;

Camera::Camera(){
	if(!_singleton){
		_singleton = true;
		m_Position = glm::vec3(-120.0f, 110.0f, 30.0f);
		m_fPhi = M_PI;
		m_fTheta = 0.0;
		computeDirectionVectors();
	}
	else{
		std::cout<<"Objet Camera déjà créé"<<std::endl;
	}
}

Camera::~Camera(){}

glm::vec3 Camera::getPosition(){
	return m_Position;
}

void Camera::computeDirectionVectors(){
	float radPhi = m_fPhi;
	float radTheta = m_fTheta;
	m_FrontVector = glm::vec3(cos(radTheta)*sin(radPhi), sin(radTheta), cos(radTheta)*cos(radPhi));
	m_LeftVector = glm::vec3(sin(radPhi+(M_PI/2.0)), 0, cos(radPhi+(M_PI/2.0)));
	m_UpVector = glm::cross(m_FrontVector, m_LeftVector);
}

void Camera::moveLeft(float t){
	m_Position += t * m_LeftVector;
	computeDirectionVectors();
}

void Camera::moveFront(float t){
	m_Position += t * m_FrontVector;
	computeDirectionVectors();
}

void Camera::rotateLeft(float degrees){
	m_fPhi += glm::radians(degrees);
	computeDirectionVectors();
}

void Camera::rotateUp(float degrees){
	m_fTheta += glm::radians(degrees);
	computeDirectionVectors();
}

glm::mat4 Camera::getViewMatrix() const{
	glm::mat4 MV = glm::lookAt(m_Position, m_Position + m_FrontVector, m_UpVector);
	return MV;
}

}
