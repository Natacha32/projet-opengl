#include "../include/Scene.hpp"

#define CLOSABLE 0
#define UNCLOSABLE 1

namespace ourLib {

void Openable::openOrClose(int comportement) {
    // openable and closable
    if(comportement == CLOSABLE)
        isOpen = !isOpen;
    // openable only once and not closable
    else
        isOpen = true; 
}

void Key::takeKey() {
    isTaken = true;
}

// constructor
Scene::Scene(std::string path, std::string base_name, std::vector<std::string> openables_name, std::string key_name) {
    // load all models
    this->base.push_back(Model(path + base_name + "/" + base_name + ".obj"));

    // initialize and load openables
    this->openables.resize(openables_name.size());
    for(unsigned int i=0; i<openables_name.size(); i++) {
        this->openables[i].closed.push_back(Model(path + openables_name[i] + "/" + openables_name[i] + ".obj"));
        // door2 est interactif mais ne peut s'ouvrir 
        if(openables_name[i]!="door2") {
            this->openables[i].open.push_back(Model(path + openables_name[i] + "/" + openables_name[i] + "_open.obj"));

        }
        this->openables[i].name = openables_name[i];
        this->openables[i].isOpen = false;
    }

    // initialize and load key
    this->key.model.push_back(Model(path + key_name + "/" + key_name + ".obj"));
    this->key.isTaken = false;
}  

// destructor
Scene::~Scene() {}

// draw the scene
void Scene::draw(Shader shader) {

    // draw the objects that are not interactive
    this->base[0].Draw(shader);

    // check if openables objects are open or closed then draw accordingly
    for(unsigned int i=0; i<this->openables.size(); i++) {
        if(!this->openables[i].isOpen) this->openables[i].closed[0].Draw(shader);
        else this->openables[i].open[0].Draw(shader);
    }

    // check if key is taken or not then draw or not
    if(this->key.isTaken == false) {
        // draw key in scene1 if not taken 
        // and key in scene2 if is not taken and if drawer is open
        if(this->openables[2].name == "drawer1") {
            this->key.model[0].Draw(shader);
        }
        else if(this->openables[2].isOpen) {
            this->key.model[0].Draw(shader);
        }
    }
}

}