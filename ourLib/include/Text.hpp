#include <SDL/SDL_ttf.h>

class Text{

   static bool _singleton;

  public :

    int id;
    SDL_Surface* texte[5];
    SDL_Color color;

    Text(); //constructeur de la classe Text

    inline int getId(){ return this->id;}

    void modifyId(int new_id, SDL_Surface* ecran, TTF_Font *police_subtitle); //modifie l'identifiant du message et le change en appelant modifyMessage

    void createText(TTF_Font *police);// initialisation du texte

    void displayTitle(SDL_Surface* ecran); //affiche le texte à l'écran

    void displayTitle(SDL_Surface* ecran, TTF_Font* police_subtitle, int compteur);

    void deleteSurfaceTitle();

    void eraseText(TTF_Font* police_title, TTF_Font* police_subtitle); // supprime le texte dans l'écran

    void animation(SDL_Surface* ecran, TTF_Font* police_title, TTF_Font* police_subtitle, int compteur);

    void createTextWindow(int status);

  private :

    std::string title;
    std::string subtitle;
    std::string subtitle_commande1;
    std::string subtitle_commande2;
    std::string subtitle_commande3;
    SDL_Rect position[6];

    void modifyTitle(int id);
};
