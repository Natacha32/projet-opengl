#ifndef CAMERA_HPP
#define CAMERA_HPP

#include "glm/glm.hpp"
#include <glm/gtc/matrix_transform.hpp>

namespace ourLib {

class Camera{

	static bool _singleton; //variable booléenne permettant de restreindre le nombre d'objets créé : design pattern Singleton

public:

	Camera(); //constructeur
	~Camera();//destructeur

	glm::vec3 getPosition();

	void computeDirectionVectors();
	void moveLeft(float t);
	void moveFront(float t); //permet d'avancer/reculer la camera
	void rotateLeft(float degrees);
	void rotateUp(float degrees);

	glm::mat4 getViewMatrix() const; //obtention de la ViewMatrix de la camera

private:

	glm::vec3 m_Position; //position de la camera
	float m_fPhi; //coordonnee spherique du FrontVector
	float m_fTheta; //coordonnee spherique du FrontVector

	glm::vec3 m_FrontVector;
	glm::vec3 m_LeftVector;
	glm::vec3 m_UpVector;

};

}

#endif
