#include "../include/Game.hpp"
#include "../include/FilePath.hpp"
#include <SDL/SDL.h>
#include "../include/Text.hpp"

namespace ourLib {

bool Game::_singleton = false;

// Constructor
Game::Game() : currentScene(1) {

    if(!_singleton){
      FilePath applicationPath;
      std::vector<std::string> openables1 = {"cabinet1", "chest1", "drawer1", "door1"};
      Scene scene1(applicationPath.file() + "../models/SCENE1_NORMAL/", "scene1", openables1, "key1");
      scenes.push_back(scene1);


      std::vector<std::string> openables2 = {"cabinet2", "chest2", "drawer2", "door2"};
      Scene scene2(applicationPath.file() + "../models/SCENE2_DARK/", "scene2", openables2, "key2");
      scenes.push_back(scene2);

      _singleton = true;
    }
    else{
      std::cout<<"Objet Game déjà créé"<<std::endl;
    }
}

// Destructor
Game::~Game() {}

// Draw the current scene that is scene 1 or scene 2
void Game::draw(Shader shader) {
    if(currentScene == 1 || currentScene ==2) {
        scenes[currentScene-1].draw(shader);
    }
}

void Game::changeScene() {
    SDL_Delay(1000);
    currentScene = 2;
}

void Game::end(Text text) {
    SDL_Delay(1000);
    currentScene = 3;
    text.createTextWindow(3);
}

}
