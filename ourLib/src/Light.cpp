#include "../include/Light.hpp"
#include <vector>

namespace ourLib {

Light::Light(){
  l_position = glm::vec3(0.0f, 0.0f, 0.0f);
}

Light::Light(glm::vec3 position){
  l_position = position;
}

/*glm::vec4 Light::lightDirection(glm::mat4 ViewMatrixCam){
  glm::vec4 LightDir = ViewMatrixCam * l_position;

  return LightDir;
}*/

glm::vec3 Light::getPosition(){
  return this->l_position;
}
}
