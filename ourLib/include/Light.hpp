#ifndef LIGHT_HPP
#define LIGHT_HPP

#include <glm/glm.hpp>
#include "glad.h"

namespace ourLib {

class Light{

  private :
    glm::vec3 l_position;

  public :
    Light();
    Light(glm::vec3 position);

    //glm::vec4 lightDirection(glm::mat4 ViewMatrixCam);
    glm::vec3 getPosition();

};

}

#endif
