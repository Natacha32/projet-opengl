#include "ourLib/include/glad.h"
#include <GLFW/glfw3.h>
#include <SDL/SDL.h>
#include <SDL/SDL_ttf.h>
#include <string>
#include <cstddef>

#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>
#include "stb_image.h"

#include "ourLib/include/Shader.hpp"
#include "ourLib/include/Camera.hpp"
#include "ourLib/include/Light.hpp"
#include "ourLib/include/Text.hpp"

#include "ourLib/include/Model.hpp"
#include "ourLib/include/Game.hpp"

#include <iostream>

using namespace ourLib;

void framebuffer_size_callback(GLFWwindow* window, int width, int height);
void processInput(GLFWwindow *window);
void handleSceneEvent(GLFWwindow *window, Scene &scene, int &currentScene, Shader pickingProgram, glm::mat4 MVP, Text text, Game &game);
int handlePicking(GLFWwindow *window, Shader pickingProgram, Scene scene, glm::mat4 MVP);

// settings
const unsigned int SCR_WIDTH = 1920;
const unsigned int SCR_HEIGHT = 1080;
char NAME[] = "ScarImac";

// camera
Camera camera;
Light Light;
float lastX = SCR_WIDTH / 2.0f;
float lastY = SCR_HEIGHT / 2.0f;
bool firstMouse = true;

// timing
float deltaTime = 0.0f;
float lastFrame = 0.0f;

#define CLOSABLE 0
#define UNCLOSABLE 1

enum { CABINET, CHEST, DRAWER, DOOR };

int main()
{
    // glfw: initialize and configure
    glfwInit();
    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
    glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);

#ifdef __APPLE__
    glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);
#endif

    // glfw window creation
    GLFWwindow* window = glfwCreateWindow(SCR_WIDTH, SCR_HEIGHT, NAME, NULL, NULL);
    if (window == NULL)
    {
        std::cout << "Failed to create GLFW window" << std::endl;
        glfwTerminate();
        return -1;
    }
    glfwMakeContextCurrent(window);
    glfwSetFramebufferSizeCallback(window, framebuffer_size_callback);

    // tell GLFW to capture our mouse
    glfwSetInputMode(window, GLFW_CURSOR, GLFW_CURSOR_NORMAL);

    // glad: load all OpenGL function pointers
    if (!gladLoadGLLoader((GLADloadproc)glfwGetProcAddress))
    {
        std::cout << "Failed to initialize GLAD" << std::endl;
        return -1;
    }

    // configure global opengl state
    glEnable(GL_DEPTH_TEST);

    // build and compile shaders
    Shader ourShader("../shaders/model_loading.vs", "../shaders/model_loading.fs");
    Shader pickingProgram( "../shaders/Picking.vs", "../shaders/Picking.fs" );

    // create objects game and file
    Game game;
    std::cout << "LOADING FINISHED" << std::endl;
    Text text;
    text.createTextWindow(0);

    // render loop
    while (!glfwWindowShouldClose(window))
    {
        // per-frame time logic
        float currentFrame = glfwGetTime();
        deltaTime = currentFrame - lastFrame;
        lastFrame = currentFrame;

        // ViewMatrix/ProjMatrix transformations
        glm::mat4 ProjMatrix = glm::perspective(glm::radians(70.f), (float)SCR_WIDTH / (float)SCR_HEIGHT, 0.1f, 600.0f);
        glm::mat4 ViewMatrix = camera.getViewMatrix();
        glm::mat4 model = glm::translate(glm::mat4(1), glm::vec3(0, 0, -5));
        glm::mat4 NormalMatrix = glm::transpose(glm::inverse(model));

        // input
        processInput(window);
        if(game.currentScene == 1 || game.currentScene == 2) {
            handleSceneEvent(window, game.scenes[game.currentScene-1], game.currentScene, pickingProgram, ProjMatrix * ViewMatrix, text, game);
        }

        // render
        glClearColor(0.05f, 0.05f, 0.05f, 1.0f);
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

        ourShader.use();

        ourShader.setMat4("uMVPMatrix", ProjMatrix * ViewMatrix);
        ourShader.setMat4("uMVMatrix",ViewMatrix);
        NormalMatrix = glm::transpose(glm::inverse(ViewMatrix));
        ourShader.setMat4("uNormalMatrix", NormalMatrix);

        game.draw(ourShader);

        // glfw: swap buffers and poll IO events (keys pressed/released, mouse moved etc.)
        glfwSwapBuffers(window);
        glfwPollEvents();
    }

    // glfw: terminate, clearing all previously allocated GLFW resources.
    glfwTerminate();
    return 0;
}

// process all input: query GLFW whether relevant keys are pressed/released this frame and react accordingly
void processInput(GLFWwindow *window)
{
    // close
    if (glfwGetKey(window, GLFW_KEY_ESCAPE) == GLFW_PRESS)
        glfwSetWindowShouldClose(window, true);

    // camera movement
    if (glfwGetKey(window, GLFW_KEY_E) == GLFW_PRESS)
        camera.moveFront(2.0);
    if (glfwGetKey(window, GLFW_KEY_R) == GLFW_PRESS)
        camera.moveFront(-2.0);
    if (glfwGetKey(window, GLFW_KEY_S) == GLFW_PRESS)
        camera.moveLeft(2.0);
    if (glfwGetKey(window, GLFW_KEY_D) == GLFW_PRESS)
        camera.moveLeft(-2.0);
    if (glfwGetKey(window, GLFW_KEY_LEFT) == GLFW_PRESS)
        camera.rotateLeft(2.0);
    if (glfwGetKey(window, GLFW_KEY_RIGHT) == GLFW_PRESS)
        camera.rotateLeft(-2.0);
    if (glfwGetKey(window, GLFW_KEY_UP) == GLFW_PRESS)
        camera.rotateUp(2.0);
    if (glfwGetKey(window, GLFW_KEY_DOWN) == GLFW_PRESS)
        camera.rotateUp(-2.0);
}

void handleSceneEvent(GLFWwindow *window, Scene &scene, int &currentScene, Shader pickingProgram, glm::mat4 MVP, Text text, Game &game) {

    /*** SCENE CHANGE ***/

    // go to scene2 if door1 is open
    if(currentScene == 1 && scene.openables[3].isOpen) {
        game.changeScene();
    }
    // end
    else if(currentScene == 2 && scene.openables[0].isOpen) {
        game.end(text);
    }

    /*** EVENTS ON CLICK ***/

    if (glfwGetMouseButton(window, GLFW_MOUSE_BUTTON_LEFT)) {

        // when clicking, search if click on interactive objects (openables or key)
        // id no id = -1, id yes id >= 0
        int id = handlePicking(window, pickingProgram, scene, MVP);

        // open objects
        if(currentScene==1) {
            if(id==CABINET || id==CHEST || id==DRAWER) {
                scene.openables[id].openOrClose(CLOSABLE);
            }
            // door unopenable unless key is taken
            else if(id==DOOR) {
                if(scene.key.isTaken) {
                    scene.openables[id].openOrClose(UNCLOSABLE);
                }
            }
        }
        else if(currentScene==2) {
            // chest
            if(id==CHEST) {
                scene.openables[id].openOrClose(CLOSABLE);
            }
            // cabinet unopenable unless key is taken
            else if(id==CABINET) {
                if(scene.key.isTaken) {
                    scene.openables[id].openOrClose(UNCLOSABLE);
                }
            }
            // door is not openable in scene 2. The player must search another object to open
            else if(id==DOOR) {
                text.createTextWindow(2);
            }
            // drawer
            else if(id==DRAWER && !scene.openables[id].isOpen) {
                scene.openables[id].openOrClose(UNCLOSABLE);
            }
            // because key is in drawer, clicking on key only clicks on drawer
            // so change condition of taking key
            // if click on drawer when drawer open, then key is taken
            else if(id==DRAWER && scene.openables[id].isOpen) {
                scene.key.takeKey();
                text.createTextWindow(1);
            }
        }

        // pick Key
        if(scene.key.isTaken==false && id==(int)scene.openables.size()) {
            scene.key.takeKey();
            text.createTextWindow(1);
        }

        // wait 200ms after clicking
        SDL_Delay(100);
    }
}


int handlePicking(GLFWwindow *window, Shader pickingProgram, Scene scene, glm::mat4 MVP) {

	// clear the screen in white
	glClearColor(1.0f, 1.0f, 1.0f, 1.0f);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	pickingProgram.use();
    pickingProgram.setMat4("MVP", MVP);

	// draw openables objects
	for(unsigned int i=0; i<scene.openables.size(); i++) {

        // convert identifiant i into a color
		float c =  float(i)/(scene.openables.size()+1);

        pickingProgram.setVec4("PickingColor", c, .0f, .0f, 1.0f);


        // draw mesh
        Model model = scene.openables[i].closed[0];
        if(scene.openables[i].isOpen) {
            Model model = scene.openables[i].open[0];
        }
        for(unsigned int j = 0; j < scene.openables[i].closed[0].meshes.size(); j++) {
            model.meshes[j].DrawColor();
        }
	}

    // draw key
    float c = (float)scene.openables.size()/(scene.openables.size()+1);
    pickingProgram.setVec4("PickingColor", c, 0.0f, 0.0f, 1.0f);

    for(unsigned int i = 0; i < 1; i++) {
        scene.key.model[0].meshes[i].DrawColor();
    }

	// wait until all the pending drawing commands are really done.
	// there are usually a long time between glDrawElements() and
	// all the fragments completely rasterized.
    glFlush();
	glFinish();
	glPixelStorei(GL_UNPACK_ALIGNMENT, 1);

	// read the pixel at the center of the screen.
	// you can also use glfwGetMousePos().
	float data[4];
    double x,y;

    glfwGetCursorPos(window, &x, &y);
	glReadPixels(x, SCR_HEIGHT-y, 1.0f, 1.0f, GL_RGBA, GL_FLOAT, data);
	// convert the color back to an integer ID

    int pickedID = data[0]*(scene.openables.size()+1);

    // if pixel clicked is white, set pickedID to -1
    if(data[0]==1 && data[1]==1 && data[2]==1) {
        pickedID = -1;
    }

    return pickedID;
}

// glfw: whenever the window size changed (by OS or user resize) this callback function executes
void framebuffer_size_callback(GLFWwindow* window, int width, int height)
{
    // make sure the Viewport matches the new window dimensions; note that width and
    // height will be significantly larger than specified on retina displays.
    glfwSetWindowTitle(window, "ScarImac");
    glViewport(0, 0, width, height);
}
