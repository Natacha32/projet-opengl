### BIBLIOTHÈQUES

Pour pouvoir compiler ce projet vous aller avoir besoin de différentes bibliothèques : 
GLFW3	
ASSIMP
SDL
SDL_TTF
SDL_IMAGE
GLM
GLAD

Voici les commandes linux pour en installer quelques unes :

sudo apt-get build-dep glfw3
sudo apt-get install libassimp-dev assimp-utils
sudo apt-get install libsdl1.2-dev
sudo apt-get install libsdl-ttf2.0-dev
sudo apt-get install libsdl-image1.2 libsdl-image1.2-dev
sudo apt-get install -y libglm-dev
Vous n'avez pas à télécharger Glad. Les fichiers nécessaires sont déjà présents.

### COMPILER ET EXÉCUTER

mkdir build
cd build
cmake ..
make
./ScarImac
