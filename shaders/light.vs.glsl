#version 330 core

//Attributs
layout (location = 0) in vec3 aPos;
layout (location = 1) in vec3 aNormal;
layout (location = 2) in vec2 aTexCoords;

out VS_OUT {
    vec3 FragPos;
    vec3 Normal;
    vec2 TexCoords;
} vs_out;

//Variables uniformes, matrices de transformations
uniform mat4 uMVPMatrix;
uniform mat4 uModelMatrix;

void main()
{
	vs_out.FragPos = aPos;
    vs_out.Normal = aNormal;
    vs_out.TexCoords = aTexCoords;
    gl_Position =uMVPMatrix * uModelMatrix * vec4(aPos, 1.0);
}
