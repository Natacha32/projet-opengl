#ifndef GAME_HPP
#define GAME_HPP

#include <vector>
#include <string>

#include "Shader.hpp"
#include "Scene.hpp"

class Text;

namespace ourLib {

class Game {
    static bool _singleton;

    public:

    int currentScene;

    std::vector<Scene> scenes;

    // constructor
    Game();
    // destructor
    ~Game();

    // draw the current scene that is scene 1 or scene 2
    void draw(Shader shader);
    // go to scene2
    void changeScene();
    // display end of game
    void end(Text text);

};

}

#endif