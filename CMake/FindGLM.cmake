#
# Try to find GLFW library and include path.
# Once done this will define
#
# GLM_FOUND
# GLM_INCLUDE_PATH
# GLM_LIBRARY
# 

IF (GLM_INCLUDE_PATH)
	SET( GLM_FOUND 1 CACHE STRING "Set to 1 if GLM is found, 0 otherwise")
ELSE (GLM_INCLUDE_PATH)
	SET( GLM_FOUND 0 CACHE STRING "Set to 1 if GLM is found, 0 otherwise")
ENDIF (GLM_INCLUDE_PATH)

MARK_AS_ADVANCED( GLM_FOUND )