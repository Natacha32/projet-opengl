#include <SDL/SDL.h>
#include <SDL/SDL_ttf.h>
#include <SDL/SDL_image.h>
#include <iostream>
#include <string>
#include <cstddef>

#include "../include/Text.hpp"

bool Text::_singleton = false;

Text::Text(){
  if(!_singleton){
    id = 0;
    modifyTitle(id);
    color = {255, 255, 255, 42};

    position[0].x = 250;
    position[0].y = 100;

    position[1].x = 200;
    position[1].y = 200;

    position[2].x = 150;
    position[2].y = 150;

    position[3].x = 150;
    position[3].y = 200;

    position[4].x = 50;
    position[4].y = 250;

    position[5].x = 100;
    position[5].y = 100;

    position[6].x = 50;
    position[6].y = 100;

    _singleton = true;

    subtitle = "Appuyez sur la touche ESPACE pour continuer";
    subtitle_commande1 = "CAMERA : E = avancer R= reculer UP = vers le haut";
    subtitle_commande2 = " DOWN = vers le bas, droite = rotation droite";
    subtitle_commande3 = "Gauche = rotation gauche, S = zoom, D = dezoom clic = prendre un objet";

  }
  else{
    std::cout<<"Objet text déjà créé"<<std::endl;
  }

}

void Text::modifyId(int new_id, SDL_Surface* ecran, TTF_Font *police_subtitle){
  this->id = new_id;
  modifyTitle(id);
  displayTitle(ecran);
  displayTitle(ecran, police_subtitle, id);
}

void Text::createText(TTF_Font *police){
  /* Écriture du texte dans la SDL_Surface texte en mode Blended (optimal) */
  texte[0] = TTF_RenderUTF8_Blended(police, title.c_str(), color);
}

void Text::displayTitle( SDL_Surface* ecran){
  if(this->id==0)
      SDL_BlitSurface(texte[0], NULL, ecran, &position[6]);
  else if(this->id==1 || this->id==2)
    SDL_BlitSurface(texte[0], NULL, ecran, &position[0]);
  else
    SDL_BlitSurface(texte[0], NULL, ecran, &position[5]); /* Blit du texte */
}

void Text::displayTitle(SDL_Surface* ecran, TTF_Font* police_subtitle, int compteur){
  if(compteur==0){
    texte[1] = TTF_RenderUTF8_Blended(police_subtitle, subtitle.c_str(), color);
    SDL_BlitSurface(texte[1], NULL, ecran, &position[1]);
  }

  if(compteur == 2){
    texte[2] = TTF_RenderUTF8_Blended(police_subtitle, subtitle_commande1.c_str(), color);
    SDL_BlitSurface(texte[2], NULL, ecran, &position[2]);

    texte[3] = TTF_RenderUTF8_Blended(police_subtitle, subtitle_commande2.c_str(), color);
    SDL_BlitSurface(texte[3], NULL, ecran, &position[3]);

    texte[4] = TTF_RenderUTF8_Blended(police_subtitle, subtitle_commande3.c_str(), color);
    SDL_BlitSurface(texte[4], NULL, ecran, &position[4]);

  }
}

void Text::deleteSurfaceTitle(){
  if(this->id ==2){
    SDL_FreeSurface(texte[0]);
    SDL_FreeSurface(texte[1]);
    SDL_FreeSurface(texte[2]);
    SDL_FreeSurface(texte[3]);
    SDL_FreeSurface(texte[4]);
  }
  else{
    SDL_FreeSurface(texte[0]);
  }

}

void Text::eraseText(TTF_Font* police_title, TTF_Font* police_subtitle){
  TTF_CloseFont(police_title);
  if(police_subtitle)
    TTF_CloseFont(police_subtitle);
  TTF_Quit();
  deleteSurfaceTitle();
}

void Text::modifyTitle(int id){

  switch(id){
    case 0:
      title = "Bienvenue dans ScarIMac.";
      break;
    case 1:
      title = "Le but : Sortir !";
      break;
    case 2:
      title = "COMMANDES";
      break;
    case 3:
      title = "Fermez cette fenêtre pour commencer le jeu.";
      break;
    case 4:
      title = "Vous avez pris la clé. Découvrez ce qu’elle ouvre !";
      break;
    case 5:
      title = "Cette porte ne peut être ouverte ! ";
      break;
    case 6:
      title = "Félicitations ! Vous êtes sorti !";
      break;
  }


}

void Text::animation(SDL_Surface* ecran, TTF_Font* police_title, TTF_Font* police_subtitle, int compteur)
{
  if(compteur < 3){
    modifyId(compteur, ecran, police_subtitle);
    createText(police_title);
  }
  else if(compteur == 3){
    modifyId(compteur, ecran, police_subtitle);
    createText(police_title);
  }
  else{
    modifyId(compteur, ecran, police_subtitle);
    createText(police_title);
  }

}

void Text::createTextWindow(int status) {
  try{
    SDL_Surface *ecran = NULL, *fond = NULL;
    SDL_Rect position_fond;
    SDL_Event event;
    TTF_Font *police_title = NULL;
    TTF_Font *police_subtitle = NULL;
    int continuer = 1;
    int compteur =0;

    SDL_Init(SDL_INIT_VIDEO);
    TTF_Init();

    ecran = SDL_SetVideoMode(600, 400, 32, SDL_HWSURFACE | SDL_DOUBLEBUF);
    SDL_WM_SetCaption("Message", NULL);
    fond = IMG_Load("../text_assets/fond.jpg");

    police_title = TTF_OpenFont("../text_assets/agencyb.TTF", 30);
    police_subtitle = TTF_OpenFont("../text_assets/agencyb.TTF", 20);
    this->createText(police_title);

    while (continuer)
    {
        SDL_WaitEvent(&event);
        switch(event.type)
        {
            case SDL_QUIT:
                continuer = 0;
                break;

            case SDL_KEYDOWN:

                case SDLK_SPACE:
                    if(compteur < 3 && status == 0){
                      compteur +=1;
                      this->animation(ecran, police_title, police_subtitle, compteur);
                    }

        }


     SDL_FillRect(ecran, NULL, SDL_MapRGB(ecran->format, 255, 255, 255));

     position_fond.x = 0;
     position_fond.y = 0;
     SDL_BlitSurface(fond, NULL, ecran, &position_fond); /* Blit du fond */

      if(status == 0){
          this->displayTitle(ecran);
          this->displayTitle(ecran, police_subtitle, this->id);
        }
        if(status ==1){
          this->animation(ecran, police_title, police_subtitle, 4);
        }
        if(status ==2){
          this->animation(ecran, police_title, police_subtitle, 5);
        }
        if(status ==3){
          this->animation(ecran, police_title, police_subtitle, 6);
        }


        SDL_Flip(ecran);

    }


    this->eraseText(police_title, police_subtitle);

    SDL_Quit();
  }
  catch (int e)
  {
    std::cout<<"An exception occured "<< e <<std::endl;
  }
  }
